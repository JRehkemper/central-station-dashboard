'use client';
import { useEffect, useState } from "react";

type Props = {
    name: string,
    link: string
}

export default function LinkCard( props: Props) {

    const [iconurl, setIconurl] = useState("");

    const fetchFaviconUrl = async () => {
        const url_without_path = processLinkUrl();
        const res = await fetch(`/api/v1/icon-finder/${url_without_path}`,
            {
                cache: "force-cache"
            });
        var link = (await res.text()).replaceAll('"','');
        if (!link.startsWith("http")) {
            link = props["link"] + link;
        }
        setIconurl(link);
    }

    const processLinkUrl = () => {
        var link_without_path;
        try {
            const link_without_schema = props["link"].split('://')[1];
            link_without_path = link_without_schema.split('/')[0];
        } catch {
            link_without_path = props["link"]
        }
        return link_without_path;
    }

    useEffect(() => {
        fetchFaviconUrl();
    }, []);

    return (
        <div className="backdrop-blur-md hover:backdrop-blur-lg backdrop-brightness-100 hover:backdrop-brightness-200 drop-shadow-xl hover:drop-shadow-2xl rounded-lg py-2 px-4 border-2 border-solid hover:scale-105 transition-all duration-300 ease-in-out">
            <a href={props["link"]} className="flex justify-start gap-2 items-center" target="_blank">
                <div>
                    <img src={iconurl} width="30px" height="30px" />
                </div>
                <div>
                    <h2 className="text-xl font-semibold">{props["name"]}</h2>
                </div>
            </a>
        </div>
    )
}
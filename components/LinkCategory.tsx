import LinkCard from "./LinkCard";

type Props = {
    category: string,
    items: Item[]
}

type Item = {
    name: string,
    link: string
}

export default function LinkCategory(props: Props) {

    return (
        <div className="flex flex-col flex-wrap gap-4 backdrop-blur-sm backdrop-brightness-95 drop-shadow-lg p-4 border-2 rounded-lg">
            <h1 className="text-2xl font-extrabold text-center">{props.category}</h1>
            {props.items.map((child) => {
                return (
                    <LinkCard name={child.name} link={child.link} key={child.name}/>
                )
            })}
        </div>
    )
}
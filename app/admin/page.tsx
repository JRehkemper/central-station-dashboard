"use client";

import { useEffect, useState } from "react"


export default function Admin() {

    const [linkForm, setLinkForm] = useState({
        "name": "",
        "link": "",
        "category": 0
    })

    const [categoryForm, setCategoryForm] = useState({
        "name": ""
    })

    const [categories, setCategories] = useState([])

    const [links, setLinks] = useState([])

    useEffect(() => {
        initiateDatabase();
        fetchCategories();
        fetchLinks();
    }, []);

    const initiateDatabase = async () => {
        await fetch('/api/v1/admin/initiate-database');
    }

    const fetchCategories = async () => {
        const res = await fetch('/api/v1/categories/all', {
            next: {
                revalidate: 0
            }
        });
        const categories = await res.json();
        setCategories(categories);
        setLinkForm({
            ...linkForm,
            "category": categories[0]["id"]
        })
    }

    const fetchLinks = async () => {
        const res = await fetch('/api/v1/links/all', {
            next: {
                revalidate: 0
            }
        });
        const links = await res.json();
        setLinks(links);
    }

    const handleLinkChange = (e: any) => {
        setLinkForm({
            ...linkForm,
            [e.target.id]: e.target.value
        })
    }

    const handleCategoryChange = (e: any) => {
        setCategoryForm({
            ...categoryForm,
            [e.target.id]: e.target.value
        })
    }


    const saveLink = async (e: any) => {
        e.preventDefault();
        await fetch('/api/v1/links/add', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
              },
            body: JSON.stringify({
                "name": linkForm["name"],
                "link": linkForm["link"],
                "category": linkForm["category"]
            }),
            next: {
                revalidate: 0
            }
        });
        fetchLinks();
        setLinkForm({
            ...linkForm,
            "name": "",
            "link": ""
        })
    }

    const deleteLink = async (id: number) => {
        await fetch('/api/v1/links/delete', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
              },
            body: JSON.stringify({
                "id": id
            }),
            next: {
                revalidate: 0
            }
        });
        fetchLinks();
    }

    const saveCategory = async (e: any) => {
        e.preventDefault();
        await fetch('/api/v1/categories/add', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
              },
            body: JSON.stringify({
                "name": categoryForm["name"]
            }),
            next: {
                revalidate: 0
            }
        });
        fetchCategories();
        setCategoryForm({
            ...categoryForm,
            "name": ""
        })
    }

    const deleteCategory = async (id: number) => {
        await fetch('/api/v1/categories/delete', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
              },
            body: JSON.stringify({
                "id": id
            }),
            next: {
                revalidate: 0
            }
        });

        fetchCategories();
    }

    return (
        <div className="min-w-screen min-h-screen p-4 flex justify-around items-center backdrop-brightness-75">
            <div className="flex flex-col gap-2 backdrop-blur-lg p-6 border-2 border-solid border-white rounded-lg">
                <h1 className="text-2xl font-bold text-center">Links</h1>
                <form className="font-black flex-col flex gap-2">
                    <input type="text" id="name" placeholder="Name" value={linkForm["name"]} onChange={handleLinkChange}></input>
                    <input type="text" id="link" placeholder="Link" value={linkForm["link"]} onChange={handleLinkChange}></input>
                    <select id="category" onChange={handleLinkChange}>
                        {categories.map((category) => {
                            return (
                                <option value={category["id"]} key={category["id"]}>{category["name"]}</option>
                            )
                        })}
                    </select>
                    <button onClick={saveLink}>Create</button>
                </form>
                <div>
                    {links.map(item => {
                        return (
                            <div className="grid grid-cols-3" key={item["id"]}>
                                <p>{item["category-name"]} </p><p> {item["name"]}</p>
                                <button onClick={() => { deleteLink(item["id"]) } }>Delete</button>
                            </div>
                        )
                    })}
                </div>
            </div>

            <div className="flex flex-col gap-2 backdrop-blur-lg p-6 border-2 border-solid border-white rounded-lg">
                <h1 className="text-2xl font-bold text-center">Categories</h1>
                <form className="font-black flex-col flex gap-2">
                    <input type="text" id="name" placeholder="Name" value={categoryForm["name"]} onChange={handleCategoryChange}></input>
                    <button onClick={saveCategory}>Create</button>
                </form>
                <div>
                    {categories.map(item => {
                        return (
                            <div className="grid grid-cols-3" key={item["id"]}>
                                <p>{item["id"]} </p><p> {item["name"]}</p>
                                <button onClick={() => { deleteCategory(item["id"]) } }>Delete</button>
                            </div>
                        )
                    })}
                </div>
            </div>
        </div>
    )
}
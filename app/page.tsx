export const dynamic = 'force-dynamic'

import LinkCategory from '@/components/LinkCategory'
import Database from '@/lib/database';

async function getDashboardData() {
    const db = new Database();

    var dashboard_links: DashboardCategory[] = [];

    const categories = await db.queryCategories();
   
    await Promise.all(
        categories.map(async (category: Category_db) => {
        var temp_object: DashboardCategory = {
            category: category["name"],
            items: await db.queryLinksInCategory(category["id"]) 
        };
        dashboard_links.push(temp_object);
        })
    );

    /* sort by category */
    dashboard_links.sort(function(a, b) { return a.category.localeCompare(b.category); });

    return dashboard_links;
}

export default async function Home() {
  const dashboard_links: DashboardCategory[] = await getDashboardData();

  return (
    <main className="flex min-h-screen flex-col flex-wrap items-center p-8 backdrop-brightness-75">
      <h1 className='text-5xl font-extrabold drop-shadow-2xl'>Central Station Dashboard</h1>
      <div className='flex flex-wrap justify-start pt-10 gap-10'>
        {dashboard_links.map((item) => {
          return (
            <LinkCategory category={item.category} items={item.items} key={item.category}/>
          )
        })}
      </div>
    </main>
  )
}

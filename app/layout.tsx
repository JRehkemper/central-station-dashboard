import './globals.css'
import type { Metadata } from 'next'
import { Inter } from 'next/font/google'
import { headers } from 'next/headers'

const inter = Inter({ subsets: ['latin'] })

export const metadata: Metadata = {
  title: 'Central Station Dashboard',
  description: 'The Dashboard for all your Links and Bookmarks',
}

export default function RootLayout({
  children,
}: {
  children: React.ReactNode
}) {

  const host = headers().get("host");


  return (
    <html lang="en">
      <body className={inter.className} style={{backgroundImage: `url(http://${host}/api/v1/static/background)`}}>{children}</body>
    </html>
  )
}

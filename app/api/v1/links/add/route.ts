import Database from "@/lib/database";
import { NextResponse } from "next/server";


export async function POST(reqest: Request) {
    const { name, link, category } = await reqest.json();

    const res = await new Database().insertLink(name, link, category);

    return NextResponse.json(res);
}
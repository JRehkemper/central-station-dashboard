import Database from "@/lib/database";
import { NextResponse } from "next/server";


export async function GET(request: Request, { params }: { params: { category: number }} ) {
    const { category } = params;

    const rows = await new Database().queryLinksInCategory(category);

    return NextResponse.json(rows);
}
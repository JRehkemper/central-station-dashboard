import Database from "@/lib/database";
import { NextResponse } from "next/server";


export async function POST(reqest: Request) {
    const { id } = await reqest.json();

    const sql = `DELETE FROM "links" WHERE "id" = ?;`;
    const values = [id];

    await new Database().run(sql, values);

    return NextResponse.json({"msg": "ok"});
}
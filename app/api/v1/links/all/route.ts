export const dynamic = 'force-dynamic';

import Database from "@/lib/database";
import { NextResponse } from "next/server";

export async function GET() {
    const rows = await new Database().queryLinks();

    return NextResponse.json(rows);
}
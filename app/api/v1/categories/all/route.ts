export const dynamic = 'force-dynamic'

import Database from "@/lib/database";
import { NextResponse } from "next/server";

export async function GET() {
    const categories = await new Database().queryCategories()
    
    return NextResponse.json(categories);
}
import Database from "@/lib/database";
import { NextResponse } from "next/server";

export async function POST(request: Request) {
    const { id } = await request.json();

    const sql = `DELETE FROM "categories" WHERE "id" = ?`;
    const values = [id];

    await new Database().run(sql, values);

    return NextResponse.json({"msg": "ok"});
}
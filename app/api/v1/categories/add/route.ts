import Database from "@/lib/database";
import { NextResponse } from "next/server"


export async function POST(request: Request) {
    const { name } = await request.json()

    await new Database().insertCategory(name);

    return NextResponse.json({"msg": "ok"})
}
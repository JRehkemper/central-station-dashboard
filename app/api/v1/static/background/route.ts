export const dynamic = "force-dynamic";

import { readFileSync } from 'fs';


export async function GET() {
    const data = readFileSync('/data/mounts/background.jpg')
    
    return new Response(data,
        {
            status: 200,
            headers: {
                "Content-Type": "image/jpeg"
            }
        })
}
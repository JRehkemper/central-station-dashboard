import { NextResponse } from "next/server"


export async function GET(request: Request, { params }: any) {
    const { url } = params

    const body = await fetchWebsite(url);

    const favicon_url = extractFaviconUrl(body);
    
    return NextResponse.json(favicon_url,
        {
            headers: {
                // one year
                'Cache-Control': 'max-age=31536000'
            }
        })
}

async function fetchWebsite(url: string) {
    process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';
    if (!url.startsWith('http')) {
        url = 'http://' + url
    }
    const res = await fetch(url, {
        next: {
            revalidate: 31536000
        }
    });
    var body = await res.text()
    return body;
}

function extractFaviconUrl(body: string) {
    /* Split on every new Tag */
    var html_lines = body.split('<');
    
    /* Collect all lines that contain the words 'rel="icon"' */
    var link_list: string[] = [];
    html_lines.forEach(line => {
        if (line.includes('rel="icon"')) {
            link_list.push(line);
        }
    });

    if (link_list.length == 0) {
        return "";
    }

    /* take the first hit and remove everything infront of the href tag */
    var favicon_line = link_list[0].split('href="')

    /* Split at the end of the link */
    favicon_line = favicon_line[1].split('"')

    /* remove trailing part and remove quotes */
    var favicon_url = favicon_line[0].replaceAll('"', '');

    if (favicon_url.startsWith("./")) {
        favicon_url = favicon_url.replace("./", "/");
    }

    /* return url */
    return favicon_url;
}
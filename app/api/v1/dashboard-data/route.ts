export const dynamic = "force-dynamic";

import Database from "@/lib/database";
import { NextResponse } from "next/server";


export async function GET() {
    const db = new Database();

    var dashboard_links: DashboardCategory[] = [];

    const categories = await db.queryCategories();
    await Promise.all(
        categories.map(async (category: Category_db) => {
        var temp_object: DashboardCategory = {
            category: category["name"],
            items: await db.queryLinksInCategory(category["id"]) 
        };
        dashboard_links.push(temp_object);
        })
    );

    return NextResponse.json(dashboard_links);
}
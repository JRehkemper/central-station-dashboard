export const dynamic = "force-dynamic"

import Database from "@/lib/database";
import { NextResponse } from "next/server";


export async function GET() {
    await new Database().initializeDatabase();

    return NextResponse.json({"msg": "ok"})
}
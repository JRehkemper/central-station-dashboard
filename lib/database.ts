const sqlite3 = require('sqlite3');
const { open } = require('sqlite');


export default class Database {
    async openDatabase() {
        const db = await open({
            filename: "/data/mounts/dashboard.db",
            //filename: "G:/Dokumente/GitHub/central-station-dashboard/central-station-dashboard/mounts/dashboard.db",
            driver: sqlite3.Database
        })
        return db;
    }

    constructor() {

    }

    async run(sql: string, values: any[]) {
        const db = await this.openDatabase();
        const res = await db.run(sql, values);
        return res;
    }

    async initializeDatabase() {
        const db = await this.openDatabase()
        await db.run(`CREATE TABLE IF NOT EXISTS "links" (
                "id" INTEGER PRIMARY KEY AUTOINCREMENT,
                "name" TEXT,
                "link" TEXT,
                "category" INTEGER 
                );`);

        await db.run(`CREATE TABLE IF NOT EXISTS categories (
            "id" INTEGER PRIMARY KEY AUTOINCREMENT,
            "name" TEXT
        );`);
        return true;
    }


    async queryCategories(): Promise<Category_db[]> {
        const query = `SELECT * FROM "categories" ORDER BY NAME;`;
        
        const db = await this.openDatabase();
        
        const rows = await db.all(query)
        return rows;
    }


    async queryLinksInCategory(category: number): Promise<Link_db[]> {
        const sql = `SELECT * FROM "links" WHERE "category" = ? ORDER BY NAME;`
        const values = [category];

        const db = await this.openDatabase();

        const rows = await db.all(sql, values);
        return rows;
    }

    async queryLinks() {
        const query = `SELECT 
                "links"."id", 
                "links"."name", 
                "links"."link",
                "links"."category",
                "categories"."name" as "category-name"
            FROM "links"
            JOIN "categories"
            ON "links"."category" = "categories"."id"
            ORDER BY "links"."category";`

        const db = await this.openDatabase();

        const rows = await db.all(query);
        return rows;
    }

    async insertCategory(name: string) {
        const db = await this.openDatabase();

        const sql = `INSERT INTO "categories" ("name") VALUES (?)`
        const values = [name];
        
        const res = await db.run(sql, values);
        return res;
    }

    async insertLink(name: string, link: string, category: number) {
        const db = await this.openDatabase();

        const sql = `INSERT INTO "links" ("name", "link", "category") VALUES (?, ?, ?);`
        const values = [name, link, category];

        const res = await db.run(sql, values);
        return res;
    }
}

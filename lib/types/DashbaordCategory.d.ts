type DashboardCategory = {
    category: string,
    items: DashboardCategoryChild[]
}

type DashboardCategoryChild = {
    name: string,
    link: string,
    id: number
}

type Link_db = {
    id: number,
    name: string,
    link: string,
    category: number    
}